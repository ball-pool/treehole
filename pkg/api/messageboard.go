package api

import (
	"time"
	"treehole/pkg/auth"
	"treehole/pkg/messageboard"

	"github.com/gin-gonic/gin"
)

type Query interface {
	FindByUserID(userID string) []MessageInfo
}

// MessageHandler _
type MessageHandler struct {
	s *messageboard.Service
	q Query
}

// NewMessageHandler _
func NewMessageHandler(s *messageboard.Service, q Query) *MessageHandler {
	return &MessageHandler{s, q}
}

// SetRouter 設定 API 路徑
func (h *MessageHandler) SetRouter(e *gin.Engine) {
	e.GET("/api/messages", h.GetMessages)
	e.POST("/api/message", h.AddMessage)
}

type MessageInfo struct {
	ID        int       `json:"id"`
	Message   string    `json:"message"`
	CreatedAt time.Time `json:"created_at"`
}

// GetMessages 取得個人留言
func (h *MessageHandler) GetMessages(c *gin.Context) {
	r := make([]messageboard.Message, 0)

	userID := c.GetHeader("X-API-Key")
	err := auth.AuthUser(userID)
	if err != nil {
		c.JSON(404, r)
		return
	}

	msgs := h.q.FindByUserID(userID)
	if len(msgs) == 0 {
		c.JSON(404, r)
		return
	}

	c.JSON(200, msgs)
}

type AddMessageData struct {
	Message string `json:"message"`
}

// AddMessage 新增留言
func (h *MessageHandler) AddMessage(c *gin.Context) {
	userID := c.GetHeader("X-API-Key")
	err := auth.AuthUser(userID)
	if err != nil {
		c.JSON(401, nil)
		return
	}

	var d AddMessageData
	err = c.BindJSON(&d)
	if err != nil {
		c.JSON(400, nil)
		return
	}

	err = h.s.Add(d.Message, userID)
	if err != nil {
		c.JSON(400, nil)
		return
	}

	c.JSON(201, nil)
}
