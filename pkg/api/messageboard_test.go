package api_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"treehole/pkg/api"
	"treehole/pkg/messageboard"
	"treehole/test"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestGetMessages(t *testing.T) {
	h := api.NewMessageHandler(
		messageboard.NewService(&test.FakeMessageRepo{}),
		&test.FakeMessageRepo{},
	)

	e := gin.Default()
	h.SetRouter(e)

	t.Run("get messages successfully", func(t *testing.T) {
		recorder := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/api/messages", nil)
		req.Header.Set("X-API-Key", "01e60c27-745c-4bfa-be59-2397f1eac4c1")
		e.ServeHTTP(recorder, req)

		assert.Equal(t, 200, recorder.Code)
		assert.JSONEq(t, `[
			{
			  "id": 1,
			  "message": "test 1",
			  "created_at": "2017-07-21T17:32:28Z"
			},
			{
			  "id": 2,
			  "message": "test 2",
			  "created_at": "2017-07-21T17:32:28Z"
			}
		  ]`, recorder.Body.String())

	})

	t.Run("should get empty response when user id is invalid", func(t *testing.T) {
		recorder := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/api/messages", nil)
		req.Header.Set("X-API-Key", "0123")
		e.ServeHTTP(recorder, req)
		assert.Equal(t, 404, recorder.Code)
		assert.JSONEq(t, `[]`, recorder.Body.String())
	})
}

func TestAddMessage(t *testing.T) {
	h := api.NewMessageHandler(
		messageboard.NewService(&test.FakeMessageRepo{}),
		&test.FakeMessageRepo{},
	)

	e := gin.Default()
	h.SetRouter(e)

	t.Run("add messages successfully", func(t *testing.T) {
		data, _ := json.Marshal(api.AddMessageData{Message: "hello world"})

		recorder := httptest.NewRecorder()
		req, _ := http.NewRequest("POST", "/api/message", bytes.NewBuffer(data))
		req.Header.Set("X-API-Key", "01e60c27-745c-4bfa-be59-2397f1eac4c1")
		e.ServeHTTP(recorder, req)

		assert.Equal(t, 201, recorder.Code)
	})

	t.Run("should return error when user is invalid", func(t *testing.T) {
		data, _ := json.Marshal(api.AddMessageData{Message: "hello world"})

		recorder := httptest.NewRecorder()
		req, _ := http.NewRequest("POST", "/api/message", bytes.NewBuffer(data))
		req.Header.Set("X-API-Key", "wrong user")
		e.ServeHTTP(recorder, req)

		assert.Equal(t, 401, recorder.Code)
	})

	t.Run("should return error when message is empty", func(t *testing.T) {
		recorder := httptest.NewRecorder()
		req, _ := http.NewRequest("POST", "/api/message", nil)
		req.Header.Set("X-API-Key", "01e60c27-745c-4bfa-be59-2397f1eac4c1")
		e.ServeHTTP(recorder, req)

		assert.Equal(t, 400, recorder.Code)
	})
}
