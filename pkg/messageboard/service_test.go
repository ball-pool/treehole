package messageboard_test

import (
	"testing"
	"treehole/pkg/messageboard"
	"treehole/test"

	"github.com/stretchr/testify/assert"
)

func TestAdd(t *testing.T) {
	fr := test.FakeMessageRepo{}
	s := messageboard.NewService(&fr)

	t.Run("add successfully", func(t *testing.T) {
		err := s.Add("hello world", "01e60c27-745c-4bfa-be59-2397f1eac4c1")
		assert.NoError(t, err)
		assert.Len(t, fr.Messages, 1)
	})

	t.Run("should return error when content is empty", func(t *testing.T) {
		err := s.Add("", "01e60c27-745c-4bfa-be59-2397f1eac4c1")
		assert.Error(t, err)
		assert.Len(t, fr.Messages, 1)
	})
}

func TestDelete(t *testing.T) {
	fr := test.FakeMessageRepo{}
	s := messageboard.NewService(&fr)

	t.Run("delete successfully", func(t *testing.T) {
		err := s.Delete(1, "01e60c27-745c-4bfa-be59-2397f1eac4c1")
		assert.NoError(t, err)
	})
}
