package messageboard

import (
	"errors"
)

type Repository interface {
	Save(msg Message) error
	Delete(msgID int, userID string) error
}

func NewService(r Repository) *Service {
	return &Service{r}
}

type Service struct {
	r Repository
}

type Message struct {
	UserID  string
	Content string
}

// Add 新增留言
func (s *Service) Add(content, userID string) error {
	if content == "" {
		return errors.New("empty content")
	}

	msg := Message{UserID: userID, Content: content}
	return s.r.Save(msg)
}

// Delete 刪除留言
func (s *Service) Delete(msgID int, userID string) error {
	return s.r.Delete(msgID, userID)
}
