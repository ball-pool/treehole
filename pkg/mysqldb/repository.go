package mysqldb

import (
	"time"
	"treehole/pkg/messageboard"

	"gorm.io/gorm"
)

// NewMessageRepository _
func NewMessageRepository(db *gorm.DB) *MessageRepository {
	return &MessageRepository{db}
}

// MessageRepository _
// 實作 messageboard 的 repository
type MessageRepository struct {
	db *gorm.DB
}

// Messages table
type Message struct {
	ID        int       `gorm:"primaryKey"`
	UserID    string    `gorm:"type:varchar(50); index; not null; column user_id; comment:使用者識別碼"`
	Content   string    `gorm:"type:varchar(360); not null; column:content; comment:留言內容"`
	CreatedAt time.Time `gorm:"type:datetime; autoCreateTime; not null; column:created_at"`
	UpdatedAt time.Time `gorm:"type:datetime; autoUpdateTime; not null; column:updated_at"`
}

// Save 寫入留言
func (r *MessageRepository) Save(msg messageboard.Message) error {
	m := Message{
		UserID:  msg.UserID,
		Content: msg.Content,
	}
	res := r.db.Create(&m)

	return res.Error
}

// Delete 刪除留言
func (r *MessageRepository) Delete(msgID int, userID string) error {
	res := r.db.Where("id = ? AND user_id = ?", msgID, userID).Delete(&Message{})
	return res.Error
}
