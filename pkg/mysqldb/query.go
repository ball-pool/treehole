package mysqldb

import (
	"treehole/pkg/api"

	"gorm.io/gorm"
)

// NewMessageQuery _
func NewMessageQuery(db *gorm.DB) *MessageQuery {
	return &MessageQuery{db}
}

// MessageQuery _
// 與管理用的 repository 做區隔，應對不同狀況的查詢使用
type MessageQuery struct {
	db *gorm.DB
}

// FindByUserID 依使用者 ID 取得所有留言
func (q *MessageQuery) FindByUserID(userID string) []api.MessageInfo {
	res := make([]api.MessageInfo, 0)
	var msgs []Message

	err := q.db.Where("user_id = ?", userID).Find(&msgs).Error
	if err != nil {
		return res
	}

	for _, v := range msgs {
		res = append(res, api.MessageInfo{
			ID:        v.ID,
			Message:   v.Content,
			CreatedAt: v.CreatedAt,
		})
	}
	return res
}
