package mysqldb_test

import (
	"testing"
	"treehole/pkg/mysqldb"

	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
)

func NewTestDB() (*gorm.DB, error) {
	return mysqldb.NewGormDB("test", "test", "mysql:3306", "test")
}

func TestNewGormDB(t *testing.T) {
	// 請先執行 docker compose 運行資料庫服務，預設使用此跑單元測試
	db, err := NewTestDB()
	assert.NoError(t, err)
	assert.NotNil(t, db)
}
