package mysqldb_test

import (
	"testing"
	"treehole/pkg/mysqldb"

	"github.com/stretchr/testify/assert"
)

func TestFindByUserID(t *testing.T) {
	db, _ := NewTestDB()
	q := mysqldb.NewMessageQuery(db)

	t.Run("set test data", func(t *testing.T) {
		err := db.Migrator().CreateTable(&mysqldb.Message{})
		assert.NoError(t, err)

		msgs := []mysqldb.Message{
			{UserID: "aaa", Content: "test1"},
			{UserID: "aaa", Content: "test2"},
			{UserID: "bbb", Content: "test3"},
		}
		err = db.Create(&msgs).Error
		assert.NoError(t, err)
	})

	t.Run("get data successfully", func(t *testing.T) {
		res := q.FindByUserID("aaa")
		assert.Len(t, res, 2)
	})

	t.Run("should return empty array when user is not found", func(t *testing.T) {
		res := q.FindByUserID("ccc")
		assert.Len(t, res, 0)
	})

	_ = db.Migrator().DropTable(&mysqldb.Message{})
}
