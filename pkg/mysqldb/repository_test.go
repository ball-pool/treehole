package mysqldb_test

import (
	"testing"
	"treehole/pkg/messageboard"
	"treehole/pkg/mysqldb"

	"github.com/stretchr/testify/assert"
)

func TestMessageRepository(t *testing.T) {
	db, _ := NewTestDB()
	r := mysqldb.NewMessageRepository(db)

	err := db.Migrator().CreateTable(&mysqldb.Message{})
	assert.NoError(t, err)

	t.Run("save message successfully", func(t *testing.T) {
		msg := messageboard.Message{
			UserID:  "999",
			Content: "hello world",
		}
		err := r.Save(msg)
		assert.NoError(t, err)
	})

	t.Run("delete message successfully", func(t *testing.T) {
		msgs := []mysqldb.Message{}
		res := db.Find(&msgs)
		assert.NoError(t, res.Error)
		assert.Len(t, msgs, 1)
		assert.Equal(t, msgs[0].ID, 1)
		assert.Equal(t, msgs[0].UserID, "999")

		err := r.Delete(1, "999")
		assert.NoError(t, err)

		res = db.Find(&msgs)
		assert.NoError(t, res.Error)
		assert.Len(t, msgs, 0)
	})

	_ = db.Migrator().DropTable(&mysqldb.Message{})
}
