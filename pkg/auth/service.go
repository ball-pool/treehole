package auth

import (
	"errors"

	"github.com/google/uuid"
)

// ErrInvalidUserID 無效的使用者 ID
var ErrInvalidUserID = errors.New("invalid user ID")

// AuthUser
// 驗證 ID 是否為有效格式
func AuthUser(id string) error {
	err := uuid.Validate(id)
	if err != nil {
		return ErrInvalidUserID
	}

	return nil
}

// GenerateUserID 產生一組不重複的 ID
func GenerateUserID() string {
	return uuid.NewString()
}
