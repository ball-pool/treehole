package auth_test

import (
	"testing"
	"treehole/pkg/auth"

	"github.com/stretchr/testify/assert"
)

func TestAuthUser(t *testing.T) {
	t.Run("should return personal ID when user is existed", func(t *testing.T) {
		userID := "01e60c27-745c-4bfa-be59-2397f1eac4c1"
		err := auth.AuthUser(userID)
		assert.NoError(t, err)
	})

	t.Run("should get error when id is invalid", func(t *testing.T) {
		err := auth.AuthUser("abc")
		assert.Error(t, err)
	})
}

func TestGenerateUserID(t *testing.T) {
	t.Run("should get new user id", func(t *testing.T) {
		id := auth.GenerateUserID()
		assert.NotEmpty(t, id)
	})
}
