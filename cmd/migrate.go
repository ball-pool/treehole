package cmd

import (
	"fmt"
	"treehole/pkg/mysqldb"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(migrateCmd)
}

var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "migrate mysql tables",
	Long:  `migrate mysql tables`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("migration start")

		db, err := mysqldb.NewGormDB("test", "test", "localhost:3306", "test")
		if err != nil {
			panic(err)
		}

		err = db.AutoMigrate(&mysqldb.Message{})
		if err != nil {
			panic(err)
		}

		fmt.Println("migration finished")
	},
}
