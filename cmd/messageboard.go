package cmd

import (
	"treehole/pkg/api"
	"treehole/pkg/messageboard"
	"treehole/pkg/mysqldb"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(messageboardCmd)
}

var messageboardCmd = &cobra.Command{
	Use:   "message-board",
	Short: "run message board API service",
	Long:  `run message board API service`,
	Run: func(cmd *cobra.Command, args []string) {
		e := gin.Default()

		db, err := mysqldb.NewGormDB("test", "test", "localhost:3306", "test")
		if err != nil {
			panic(err)
		}

		mh := api.NewMessageHandler(
			messageboard.NewService(mysqldb.NewMessageRepository(db)),
			mysqldb.NewMessageQuery(db),
		)
		mh.SetRouter(e)

		err = e.Run(":80")
		if err != nil {
			panic(err)
		}
	},
}
