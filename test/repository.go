package test

import (
	"time"
	"treehole/pkg/api"
	"treehole/pkg/messageboard"
)

type FakeMessageRepo struct {
	Messages []messageboard.Message
}

func (f *FakeMessageRepo) Save(msg messageboard.Message) error {
	f.Messages = append(f.Messages, msg)
	return nil
}

func (f *FakeMessageRepo) Delete(msgID int, userID string) error {
	return nil
}

func (f *FakeMessageRepo) FindByUserID(userID string) []api.MessageInfo {
	time, _ := time.Parse(time.RFC3339, "2017-07-21T17:32:28Z")
	return []api.MessageInfo{
		{ID: 1, Message: "test 1", CreatedAt: time},
		{ID: 2, Message: "test 2", CreatedAt: time},
	}
}
